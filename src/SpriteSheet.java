import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class SpriteSheet implements Component {

	private String entityTag;
	private String src;
	private BufferedImage[][] images;
	private Vector2D dimensions;
	private int delay;
	private boolean isAlive=true, sameAnimation=false;
	private Vector2D offset;
	private int currentAnimation=0;
	private ArrayList<Animation> animations = new ArrayList<Animation>();
	private int frameRows, frameCols;
	
	public SpriteSheet(Entity e, String imgSrc, int destWidth, int destHeight, 
			int frameWidth, int frameHeight, int delay){
		this.entityTag=e.getTag();
		src=imgSrc;
		loadImages(frameWidth, frameHeight);
		dimensions=new Vector2D(destWidth, destHeight);
		this.delay=delay;
		offset=new Vector2D(0,0);
		
		animations.add(new Animation(images.length, "example"));
		for(int i=0; i<images.length; i++)
			animations.get(0).setFramePosition(i, 0, i);
		
		animations.add(new Animation(images.length, "backWards"));
		for(int i=images.length-1; i>=0; i--)
			animations.get(1).setFramePosition(i%2, 0, i);
		
		animationThread.start();
	}
	
	
	
	private void loadImages(int width, int height){
		BufferedImage srcImage = null;
		try{
			srcImage = javax.imageio.ImageIO.read(new File(src));
		} catch (IOException ex) {
			Game.log("ERROR: IO exception when loading SpriteSheet: '" 
					+ src + "' \n" + ex);
		}
		
		// split srcImag into a grid depending on frame size
		
		frameCols = srcImage.getWidth()/width;
		frameRows = srcImage.getHeight()/height; 
		images= new BufferedImage[frameCols][frameRows];
		
		if(srcImage!=null)
		for(int y=0; y<frameRows; y++)
			for(int x=0; x<frameCols; x++){
				images[x][y]= 
						srcImage.getSubimage(x*width, y*height, 
							width, height);
			}
	}
	
	@Override
	public void initiate() {
		
	}

	@Override
	public void update() {
	}
	
	public void changeAnimation(String aniName){
		//if changing to current animation
		if(getCurrentAni().getName().equalsIgnoreCase(aniName))
			return; 
		
		int index=getAnimation(aniName);
		if(index!=-1){
			currentAnimation=index;
			//reset animation before we use it
			getCurrentAni().resetCount(); 
			//tell animation thread we changed the animation
			sameAnimation=false; 
		}
	}
	
	private int getAnimation(String aniName) {
		for(int i=0; i<animations.size(); i++){
			if(animations.get(i).getName().equalsIgnoreCase(aniName))
				return i;
		}
		return -1; //not found
	}

	Thread animationThread =new Thread(){
		public void run(){
			while(isAlive){
				/* if just after starting a new  animation
					stay on first frame of the delay time 
					and ignore delay time of the last animation 
				*/
				if(sameAnimation==false){ 
					sameAnimation=true;
					try{ sleep(delay); }
					catch(InterruptedException ex){
						Game.log("SpriteSheet thread interupted");
						ex.printStackTrace();
					}
				}
				else{
					getCurrentAni().nextFrame();
				try{ sleep(delay); }
					catch(InterruptedException ex){
						Game.log("SpriteSheet thread interupted");
						ex.printStackTrace();
					}
				}
			}
		}
	};

	@Override
	public String getTag() {
		return "SPRITESHEET";
	}

	@Override
	public void move(Vector2D amount) {
		
	}

	@Override
	public String getEntityTag() {
		return entityTag;
	}
	
	private Animation getCurrentAni(){
		return animations.get(currentAnimation);
	}

	public BufferedImage getImage() {
		if(images.length<=0 || images==null){
			Game.log("SpriteSheet Images empty");
			return null;	
		}
		
		return images[getCurrentAni().getFramePosX()][getCurrentAni().getFramePosY()];
	}
	
	public Vector2D getOffset(){
		return offset;
	}

	public Vector2D getDimensions() {
		return dimensions;
	}

}
