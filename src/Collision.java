import java.util.ArrayList;


public class Collision implements Component {

	private boolean isStatic;
	private boolean isTrigger;
	private ArrayList<String> oldEntityEvents = new ArrayList<String>(); //this hold triggers from last frame
	private ArrayList<String> entityEvents = new ArrayList<String>(); //this is updated every frame
	private Vector2D position;
	private String entityTag="";
	public Vector2D getPosition(){return position;}
	public Vector2D getOrigin(){return null;}
	
	public Collision(Entity e, boolean isStatic, boolean isTrigger){
		position=e.getPosition2D();
		entityTag=e.getTag();
		this.isStatic =isStatic;
		this.isTrigger = isTrigger;
	}
	
	public Collision(Tile e, boolean isStatic, boolean isTrigger){
		entityTag="Tile" + e.getPosition().toString();
		position=e.getPosition().multi(new Vector2D(Tile.WIDTH, Tile.HEIGHT));
		this.isStatic =isStatic;
		this.isTrigger = isTrigger;
	}
	
	public Collision(Vector2D position, boolean isStatic, String entityTag, boolean isTrigger){
		this.entityTag= entityTag;
		this.position = position;
		this.isStatic =isStatic;
		this.isTrigger = isTrigger;
	}
	
	public String getType(){
		return null;
	}
	
	@Override
	public void initiate() {}

	@Override
	public void update() {
		//Game.log(entityTag + ": old:"+ oldEntitysTriggering.size()+ ",new:" + entitysTriggering.size());
		
			if(oldEntityEvents.size()!=entityEvents.size()){ //if enter or exit has happened
				ArrayList<String> enters = new ArrayList<String>();
				ArrayList<String> exits = new ArrayList<String>();
				
				
				
				//find all different entitys in news, triggerEnter
				for(String s : entityEvents)
					if(!oldEntityEvents.contains(s))
						enters.add(s);
				
				//find all different entitys in old, triggerExit
				for(String s : oldEntityEvents){
					if(!entityEvents.contains(s))
						exits.add(s);
				}
				
				for(String s : enters){
					if(this.isTrigger)
						onTriggerEnter(s);
					else onCollisionEnter(s);
					oldEntityEvents.add(s);
				}
				
				for(String s : exits){
					if(this.isTrigger)
						onTriggerExit(s);
					else onCollisionExit(s);
					oldEntityEvents.remove(oldEntityEvents.indexOf(s));
				}
				
			}
			entityEvents.clear();
	}

	public void onCollisionExit(String entityTag) {
		Game.log(this.entityTag + " Collision exited by:" + entityTag);
	}

	public void onCollisionEnter(String entityTag) {
		Game.log(this.entityTag + " Collision entered by:" + entityTag);
	}

	public void onTriggerExit(String entityTag) {
		Game.log(this.entityTag + " trigger exited by:" + entityTag);
	}

	public void onTriggerEnter(String entityTag) {
		Game.log(this.entityTag + " trigger entered by:" + entityTag);
	}

	@Override
	public String getTag() {return "COLLISION"; }

	@Override
	public void move(Vector2D amount) {
		position.add(amount);
	}
	
	public void setIsStatic(boolean isStatic){
		this.isStatic=isStatic;
	}
	
	public boolean getIsStatic(){
		return isStatic;
	}
	
	public boolean intersects(Collision collison){ 
		Game.log("Collision not checked, must be defined in subclass");
		return false;
		}

	public Vector2D depth(Collision collision) {
		Game.log("depth not returned, must be defined in subclass");
		return null;
	}

	@Override
	public String getEntityTag() {
		return entityTag;
	}

	public boolean getIsTrigger() {
		return isTrigger;
	}

	public void event(String entityTag) {
			entityEvents.add(entityTag);
	}

}
