
//2d coordinates
public class Vector2D {
	private int x, y;
	public int X(){return x;}
	public int Y(){return y;}
	
	public Vector2D(int x, int y){
		this.x=x;
		this.y=y;
	}
	
	public Vector2D() {
		x=0; y=0;
	}
	public void move(int x, int y){
		this.x += x;
		this.y += y;
	}
	
	public void move(Vector2D amount){
		this.x += amount.X();
		this.y += amount.Y();
	}
	
	public void reset(){
		x=0; y=0;
	}
	public void add(Vector2D playerMovement) {
		x+=playerMovement.X();
		y+=playerMovement.Y();
	}

	public void scale(float scale){
		x=(int)(x*scale);
		y=(int)(y*scale);
	}
	
	public void scale(int scale){
		x*=scale;
		y*=scale;
	}
	public Vector2D distanceAsVector2D(Vector2D vector) {
		return new Vector2D(vector.x-x, vector.y-y);
	}
	
	public Vector2D normalised(){
		int total = x+y;
		float newx=x/total, newy=y/total;
		return new Vector2D(Math.round(newx), Math.round(newy));
	}
	
	public double distance(Vector2D pos) {
		return Math.sqrt((x-pos.x)*(x-pos.x) + (y-pos.y)*(y-pos.y));
	}
	
	public String toString(){
		return "(" + x + "," + y +")";
	}
	public Vector2D multi(Vector2D vector) {
		return new Vector2D(x*vector.X(), y*vector.Y());
	}
	public Vector2D multi(int amount) {
		return new Vector2D(x*amount, y*amount);
	}
	public Vector2D minus(Vector2D position) {
		return new Vector2D(x-position.X(), y-position.Y());
	}
}
