
public class Dialog {
	
	private String text;
	private int id;
	
	public Dialog(String text, int id){
		this.text=text;
		this.id=id;
	}
	
	public String text(){return text;}
	public int id(){return id;}
}
