import java.util.ArrayList;


public class InputReader {
	private boolean runningGame=true;
	public boolean getRunningGame(){return runningGame;}
	private ArrayList<Integer> keysPressed = new ArrayList<Integer>();
	private ArrayList<Integer> keysClicked = new ArrayList<Integer>();
	private ArrayList<Integer> keysReleased = new ArrayList<Integer>();
	
	private Vector2D directionInput = new Vector2D(0,0);
	
	//default direction keys in unicode
	final int UP1=87, UP2=38, 
			DOWN1=83, DOWN2=40,
			LEFT1=65, LEFT2=37,
			RIGHT1=68, RIGHT2=39;
	
	//more default buttons
	final int ESC=27;
	
	
	public InputReader(){}

	public void read(ArrayList<Integer> input) {
		directionInput.reset();
		keysPressed = input;
		getKeysClickedandReleased(input);
		
		for(int i=0; i<input.size(); i++){
			switch(input.get(i)){
				case ESC : runningGame=false; break; //escape
				case UP2 : case UP1 : directionInput.move(0,-1); break; //up,w
				case LEFT2 : case LEFT1 : directionInput.move(-1,0); break; //left,a
				case RIGHT2 : case RIGHT1 : directionInput.move(1,0); break; //right,d
				case DOWN2 : case DOWN1 : directionInput.move(0, 1); break; //down,s
			}
		}
	}
	
	//decide when should be added and removed from clicked and released arraylists
	private void getKeysClickedandReleased(ArrayList<Integer> input) {
		
		//remove any released keys from previous frame
		for(int i=0; i<keysReleased.size(); i++){
			if(isKeyUp(keysReleased.get(i))){
				keysReleased.remove(i);
			}
		}
		
		//log any released keys
		//when released remove it from being clicked
		for(int i=0; i<keysClicked.size(); i++){
			if(isKeyUp(keysClicked.get(i))){
				keysReleased.add(keysClicked.get(i));
				keysClicked.remove(i);
			}
		}
	}

	/*
	 ===== if character is passed as keyCode, it must be upper case =====
	*/
	
	//if key is pressed down
	public boolean isKeyDown(int keyCode){ 
		return keysPressed.contains(keyCode);
	}
	
	//if key is not pressed
	public boolean isKeyUp(int keyCode){
		return !keysPressed.contains(keyCode);
	}
	
	//if button is clicked
	public boolean isKeyClicked(int keyCode){
		if(keysPressed.contains(keyCode) && 
				keysClicked.contains(keyCode)==false ){
			keysClicked.add(keyCode);
			Game.log("clicked:" + keyCode);
			return true;
		}
		return false;
	}
	
	//if button is released
	public boolean isKeyReleased(int keyCode){
		for(int i=0; i<keysReleased.size(); i++){
			if(isKeyUp(keysReleased.get(i))){
				keysReleased.remove(i);
				Game.log("relased:" + keyCode);
				return true;
			}
		}
		return false;
	}
	
	//getters for directional input
	public int getHorizontal(){return directionInput.X();}
	public int getVertical(){return directionInput.Y();}
	public Vector2D getDirection(){return directionInput;}

}
