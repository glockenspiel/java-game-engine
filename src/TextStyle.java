import java.awt.Color;
import java.awt.Font;


public class TextStyle {
	private Color color;
	private Font font;
	
	public TextStyle(Color color, String font, int size){
		this.color=color;
		this.font=new Font(font, Font.PLAIN, size);
	}
	
	public TextStyle(Color color, String font, int size, int fontType){
		this.color=color;
		this.font=new Font(font, fontType, size);
	}
	
	public Color color(){ return color; }
	public Font font(){ return font; }
}
