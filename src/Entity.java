import java.awt.image.ComponentSampleModel;
import java.util.ArrayList;


public class Entity implements EntityInterface {

	private ArrayList<Component> components = new ArrayList<Component>();
	//private ArrayList<CollisionReference> collisions = new ArrayList<CollisionReference>();
	private Transform2D transform = new Transform2D();
	private String tag="";
	
	
	public Entity(){}
	
	public Entity(String tag){this.tag=tag;}
	
	@Override
	public void initiate() {
		for(Component c : components)
			c.initiate();
	}
	
	@Override
	public void update() {
		for(Component c : components){
			c.update();
			
		}
	}
	
	//getter and setters
	@Override
	public ArrayList<Component> getComponents() {return components;}

	@Override
	public void addComponent(Component c) {components.add(c);}

	@Override
	public Transform2D getTransform2D() {return transform;}

	@Override
	public Vector2D getPosition2D() {return transform.getPosition2D();}
	
	@Override
	public void setPosition2D(Vector2D position) {
		Vector2D moveAmount = position.minus(getPosition2D());
		move(moveAmount);
	}

	@Override
	public int getRotation() {return transform.getRotation();}

	@Override
	public void setRotation(int rotation) {transform.setRotation(rotation);}
	
	@Override
	public Vector2D getScale2D() {return transform.getScale2D();}

	@Override
	public void setScale2D(Vector2D scale) {transform.setScale2D(scale);}

	@Override
	public String getTag() {return tag;}
	
	@Override
	public void setTag(String tag) {this.tag=tag;}

	@Override
	public Component getComponentByTag(String tag) {
		for(int i=0; i<components.size(); i++)
			if(components.get(i).getTag()==tag)
				return components.get(i);
		return null;
	}

	@Override
	public void move(Vector2D amount) {
		//transform.move(amount);
		for(Component c : components)
			c.move(amount);
	}

	@Override
	public ArrayList<Component> getComponentsByTag(String tag) {
		ArrayList<Component> components = new ArrayList<Component>();
		if(components.size()>0 && this.components.size()>0)
		for(int i=0; i<this.components.size(); i++)
			if(this.components.get(i).getTag()==tag)
				components.add(components.get(i));
		
		return components;
	}

	@Override
	public ArrayList<Collision> getCollisions() {
		ArrayList<Collision> colls = new ArrayList<Collision>();
		
		if(components.size()>0)
		for(Component c: components){
			
			if(c.getClass().getSuperclass().equals(Collision.class)){
				colls.add((Collision)c);
			}
			else if(c.getClass().equals(Collision.class)){
				colls.add((Collision)c);
			}
				
		}
		return colls;
	}

	@Override
	public ArrayList<Component> getComponentsByType(String type) {
		ArrayList<Component> comps = new ArrayList<Component>();
		
		if(components.size()>0)
		for(Component c: components){
			if(c.getClass().getSuperclass().toString().equals(type) || c.getClass().toString().equals(type)){
				comps.add(c);
			}
			
		}
		return comps;
	}

	@Override
	public Vector2D getOrigin2D() {
		return transform.getOrigin();
	}
}
