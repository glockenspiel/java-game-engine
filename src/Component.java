
public interface Component {
	public void initiate();
	public void update();
	public String getTag();
	public void move(Vector2D amount);
	public String getEntityTag();
}
