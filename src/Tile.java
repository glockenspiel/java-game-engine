

public class Tile { //Tile or block
	
	protected static final int WIDTH=20,HEIGHT=20;
	private Vector2D position;
	private int index;
	
	/*constructors
		if no Collision or boolean passed: hasCollision=false
		if Collision is passed: hasCollision=true;
		if true boolean is passed: Collision box is generated by default
	*/
	Tile(int x, int y, int index){
		position=new Vector2D(x,y);
		this.index=index;
	}
	
	Tile(Vector2D position, int index){
		this.position=position;
		this.index=index;
	}
	
	//END OF CONSTRUCTORS

	//GETTERS
	public Vector2D getPosition(){
		return position;
	}
	public int getX(){
		return position.X();
	}
	
	public int getY(){
		return position.Y();
	}
	
	public int getIndex(){
		return index;
	}
}
