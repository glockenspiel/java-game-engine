import java.util.ArrayList;


public class CollisionManager {

	public CollisionManager(){}
	
	public void resolve(ArrayList<Collision> colls){
		
		/*
		 * should use quad trees rather than brute force, x10 less comparisons, look here:
		 * http://gamedevelopment.tutsplus.com/tutorials/quick-tip-use-quadtrees-to-detect-likely-collisions-in-2d-space--gamedev-374
		 */
		
		//find all intersections and notify components of collision and/or trigger
			if(colls.size()>1)
			for(int i=0; i<colls.size()-1; i++)
				for(int j=i+1; j<colls.size(); j++)
					if(colls.get(i).intersects(colls.get(j))){
						//1 is trigger
						trigger(colls.get(i), colls.get(j));
						
						// 1 is static
						if(colls.get(i).getIsStatic() || colls.get(j).getIsStatic()){
							
							//which is static and dynamic
							if(!colls.get(i).getIsStatic())
								dynamicStatic(colls.get(i), colls.get(j));
							else 
								dynamicStatic(colls.get(j), colls.get(i));
						}
					
					}
	}

	private void trigger(Collision a, Collision b) {
		//a is trigger
		if(a.getIsTrigger() && !b.getIsTrigger())
			a.event(b.getEntityTag());

		//b is trigger
		if(!a.getIsTrigger() && b.getIsTrigger())
			b.event(a.getEntityTag());
	}

	//collision resolve of static and dynamic
	private void dynamicStatic(Collision dynamicColl, Collision staticColl) {
		Vector2D depth = dynamicColl.depth(staticColl);
		if(depth!=null){
			//depth on 1 axis only, the smaller one
			int xsmaller = Math.abs(depth.X())-Math.abs(depth.Y());
			if(xsmaller<0) //x is smaller
				 depth=new Vector2D(depth.X(),0);
			else if(xsmaller>0) //y is smaller
				depth= new Vector2D(0,depth.Y());
			//this case stops sticky collision
			else //equal
				depth= new Vector2D(0,0); 
			
			
			//find entity and move it
			String eTag= dynamicColl.getEntityTag();
			int eIndex = Scene.getEntityIndexByTag(eTag);
			if(eIndex!=-1) Scene.getEntitys().get(eIndex).move(depth); //move by depth
			else{Game.log("entity not found:" + eTag);}
			
			//notify of collision
			dynamicColl.event(staticColl.getEntityTag());
			staticColl.event(dynamicColl.getEntityTag());
		}
	}
	
}
