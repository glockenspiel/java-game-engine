import java.util.ArrayList;


public class DialogTree implements Component {
	
	private String entityTag;
	private String actor;
	private ArrayList<Dialog> tree = new ArrayList<Dialog>();
	private int count=0;
	private int currentBranch=0;
	private boolean isTalking=false;
	
	public DialogTree(String entityTag, String actor){
		this.entityTag=entityTag;
		this.actor=actor;
	}
	
	public String getActor(){
		return actor;
	}
	
	public void addBranch(String text){
		tree.add(new Dialog(text,count));
		count++;
	}
	
	//get current text
	public String curText(){
		if(!isTalking)
			return null;
		return tree.get(currentBranch).text();
	}
	
	public String getPrompt(){
		return "Talk to " + actor;
	}
	
	public void nextBranch(){
		currentBranch++;
		if(currentBranch>=tree.size()){
			isTalking=false;
			currentBranch=0;
		}
	}
	
	public void startTalking(){
		isTalking=true;
	}
	
	public boolean IsTalking(){
		return isTalking;
	}
	

	@Override
	public void initiate() {}

	@Override
	public void update() {
	}

	@Override
	public String getTag() {
		return "DIALOG";
	}

	@Override
	public void move(Vector2D amount) {
	}

	@Override
	public String getEntityTag() {
		return entityTag;
	}

	public void stopTalking() {
		isTalking=false;
	}

}
