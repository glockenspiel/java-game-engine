
public class Transform2D {
	private Vector2D position;
	private Vector2D scale;
	private Vector2D originOffset;
	private int rotation;
	
	//each entity has a transform
	
	//constructors
	public Transform2D(){
		position = new Vector2D(0,0);
		rotation=0;
		scale = new Vector2D(0,0);
		originOffset = new Vector2D(32,32);
	}
	
	public Transform2D(Vector2D position){
		this.position=position;
		rotation=0;
		scale = new Vector2D(0,0);
		originOffset = new Vector2D(32,32);
	}
	
	public Transform2D(Vector2D position, int rotation){
		this.position=position;
		this.rotation = rotation;
		scale = new Vector2D(0,0);
		originOffset = new Vector2D(32,32);
	}
	
	public Transform2D(Vector2D position, int rotation, Vector2D scale){
		this.position=position;
		this.rotation = rotation;
		this.scale=scale;
		originOffset = new Vector2D(32,32);
	}
	
	
	public Vector2D getPosition2D(){
		return position;
	}
	public void setPosition2D(Vector2D position){
		this.position=position;
	}
	public void move(Vector2D amount){
		position.add(amount);
	}
	
	
	public Vector2D getScale2D(){
		return scale;
	}
	public void setScale2D(Vector2D scale){
		this.scale=scale;
	}
	public void scaleBy(float amount){
		scale.scale(amount);
	}
	public void scaleBy(int amount){
		scale.scale(amount);
	}
	
	
	public int getRotation(){
		return rotation;
	}
	public void setRotation(int rotation){
		this.rotation=rotation;
	}
	public void rotate(int angle){
		rotation+=angle;
	}

	public Vector2D getOrigin() {
		return new Vector2D(position.X()+ originOffset.X(),
				position.Y()+ originOffset.Y());
	}
	
	 
}
