import java.util.ArrayList;

public interface EntityInterface {
	public ArrayList<Component> getComponents();
	public void addComponent(Component c);
	public void initiate();
	public void update();
	public Transform2D getTransform2D();
	public Vector2D getPosition2D();
	public void setPosition2D(Vector2D position);
	public int getRotation();
	public void setRotation(int rotation);
	public Vector2D getScale2D();
	public void setScale2D(Vector2D scale);
	public String getTag();
	public void setTag(String tag);
	public Component getComponentByTag(String tag);
	public ArrayList<Component> getComponentsByTag(String tag);
	public void move(Vector2D amount);
	public ArrayList<Collision> getCollisions();
	public ArrayList<Component> getComponentsByType(String type);
	public Vector2D getOrigin2D();

}
