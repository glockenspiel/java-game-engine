import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TileMap {
	private int width, height;
	private List<List<Tile>> map = new ArrayList<List<Tile>>();
	private ArrayList<Legend> legend = new ArrayList<Legend>();
	private final String tag="TILEMAP";
	
	//blank tilemap to fill screen
	//not a good idea to mix this with camera
	public TileMap(){ 
		width=(int)Game.FRAME_WIDTH/Tile.WIDTH + 1;
		height=(int)Game.FRAME_HEIGHT/Tile.HEIGHT +1;
		clearMap();
	}
	
	//empty constructor with size set
	public TileMap(int x, int y){
		width=x;
		height=y;
		clearMap();
	}
	
	public TileMap(String filePrefix){
		loadMap(filePrefix+"_indexes.csv", filePrefix+"_legend.csv");
	}
	
	public TileMap(String indexesFilename, String legendFilename){
		loadMap(indexesFilename, legendFilename);
	}
	
	private void loadMap(String indexesFilename, String legendFilename) {
		ReadCSV readcsv = new ReadCSV();
		
		//load legend
		ArrayList<ArrayList<String>> legendcsv = readcsv.getStringData(legendFilename);
		for(int i=0; i<legendcsv.size(); i++){
			legend.add(new Legend(	legendcsv.get(i).get(0), 
									legendcsv.get(i).get(1),
									legendcsv.get(i).get(2),
									legendcsv.get(i).get(3))
						);
		}
		
		//load map indexes
		ArrayList<ArrayList<Integer>> indexes = readcsv.getData(indexesFilename);
		
		//set map size
		height=indexes.size();
		width=indexes.get(0).size();
		
		//use legend to convert indexes->Tiles and fill map
		for(int y=0; y<indexes.size(); y++){
			ArrayList<Tile> tileRow = new ArrayList<Tile>();
			for(int x=0; x<indexes.get(y).size(); x++){
				Legend l = getLegendByIndex(indexes.get(y).get(x));
				if(l!=null){
					tileRow.add(new Tile(x, y, l.INDEX)); //add tile to row
				}
				else{ 
					tileRow.add(blankTile(x,y));
					System.out.println("TileMap.loadMap(): tile is blank (x,y): "
										+ x +"," + y);
				}
			}
			map.add(tileRow);
		}	
	}

	//Initialise all Tiles to index -1
	private void clearMap(){
		map.clear();
		for(int y=0; y<height; y++){
			ArrayList<Tile> row = new ArrayList<Tile>();
			for(int x=0; x<width; x++){
				row.add(blankTile(x,y));
			}
			map.add(row);
		}
	}
	
	//getters
	public Tile getTile(int x,int y){
		return map.get(y).get(x);
	}
	
	public Tile getTile(Vector2D pos){
		return map.get(pos.Y()).get(pos.X());
	}
	
	public int getWidth(){
		return width;
	}
	
	public int getHeight(){
		return height;
	}
	
	private Legend getLegendByIndex(int index){
		for(Legend l : legend)
			if(l.INDEX==index){
				return l;
			}
		
		return null;
	}
	
	public ArrayList<Collision> getAllCollisions(){
		ArrayList<Collision> colls = new ArrayList<Collision>();
		for(int y=0; y<map.size(); y++)
			for(int x=0; x<map.get(y).size(); x++){
				if(tileHasCollision(x,y))
					colls.add(new CollisionBox(getTile(x,y),true, tileIsTrigger(x,y)));
			}
		return colls;
		}
		
	private boolean tileIsTrigger(int x, int y) {
		return getLegendByIndex(getTile(x,y).getIndex()).isTrigger();
	}

	public boolean tileHasCollision(int x, int y) {
		return getLegendByIndex(getTile(x,y).getIndex()).hasCollision;
	}
	
	private Tile blankTile(int x, int y){
		return new Tile(x,y,-1);
	}
}
