import java.awt.Point;
import java.awt.Rectangle;


public class Camera implements Component {
	
	private Rectangle bounds;
	
	//larger value is smoother & slower camera, limted from 0-100
	private Vector2D smoothing; 
	
	private String entityTag;
	private boolean isSticky=false;
	private Vector2D startPosition;
	public Vector2D getOrigin(){return new Vector2D((int)bounds.getCenterX(), (int)bounds.getCenterY()); }
	
	private Vector2D offset=new Vector2D();
	public Vector2D getOffset(){
		return new Vector2D((int)bounds.getCenterX()-Game.FRAME_WIDTH/2, (int)bounds.getCenterY()-Game.FRAME_HEIGHT/2);
		//return offset.minus(startPosition); 
	}
	
	public Camera(Rectangle bounds, Vector2D smoothing, 
			String entityTag, Vector2D entityOrigin){
		this.bounds=bounds;
		clampSmoothing(smoothing);
		this.entityTag=entityTag;
		findStartPos(entityOrigin);
	}

	public Camera(int width, int height, Vector2D smoothing, String entityTag, Vector2D entityOrigin){
		this.bounds=new Rectangle(entityOrigin.X()-width/2,entityOrigin.Y()-height/2,width,height);
		clampSmoothing(smoothing);
		this.entityTag=entityTag;
		findStartPos(entityOrigin);
	}

	private void clampSmoothing(Vector2D smoothing) {
		int x=smoothing.X(), y=smoothing.Y();
		x=Mathg.clamp(x, 100, 0);
		y=Mathg.clamp(y, 100, 0);
		this.smoothing=new Vector2D(x,y);
	}
	
	private void findStartPos(Vector2D entityOrigin) {
		//get center point of screen and move it to the origin
		int x = Game.FRAME_WIDTH/2 - entityOrigin.X();
		int y = Game.FRAME_HEIGHT/2 - entityOrigin.Y();
		startPosition=new Vector2D(x,y);
	}

	@Override
	public void initiate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getTag() {
		return "CAMERA";
	}

	@Override
	public void move(Vector2D amount) {
		//bounds.setLocation(bounds.x+amount.X(), bounds.y+amount.Y());
		offset.add(amount);
	}

	@Override
	public String getEntityTag() {
		return entityTag;
	}

	@Override
	public void update() {
		//get player origin and compute new position of camera
		if(!isSticky){
			//disable for now, still doing sticky camera
			Vector2D followOrigin = Scene.getEntityByTag(entityTag).getOrigin2D();
			
			if(!bounds.contains
					(new Point(followOrigin.X(), 
							followOrigin.Y()))){
				Vector2D newPosition = newCameraPosition(
						Scene.getEntityByTag(entityTag).getOrigin2D());
				bounds.setLocation(newPosition.X(), newPosition.Y());
			}
		}
	}
	
	// + zoom in, - zoom out
	public void zoom(double zoomAmount){
		
	}
	
	public void resetToOrigin(){
		//move to center point of bounds
		
	}
	
	public void setSticky(boolean isSticky){
		this.isSticky=isSticky;
	}
	public boolean getIsSticky(){
		return isSticky;
	}
	
	public void shake(int angle, int movementX, int movementY){
		//shake camera to angle
		//movement noise with bounds of +/- X and Y
	}
	
	private Vector2D newCameraPosition(Vector2D entityOrigin){
		//calculate lerped position
		//linear interpolation based on the following formula
		//value1 + (value2-value1) * smoothingAmount(secs)
		int x=(int)bounds.getCenterX();
		int y=(int)bounds.getCenterY();
		float finx = bounds.x;
		float finy = bounds.y;
		finx += (entityOrigin.X()-x)* (1/(float)smoothing.X());
		finy += (entityOrigin.Y()-y)* (1/(float)smoothing.Y());
		
		return new Vector2D(Math.round(finx),Math.round(finy));
	}

	public Rectangle getViewport() {
		return new Rectangle(); //todo
	}

	public Rectangle getBounds() {
		return bounds;
	}
}
