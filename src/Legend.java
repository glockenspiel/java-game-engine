
public class Legend {
	public final int INDEX;
	public final String NAME;
	public final boolean hasCollision; //if tile has collision it is of type static
	public final String IMAGE_SRC;
	private boolean isTrigger;
	
	public Legend(int index, String name, String collisionType, String isTrigger){
		INDEX = index;
		NAME = name;
		this.hasCollision=!collisionType.equalsIgnoreCase("None");
		IMAGE_SRC = "";
		this.isTrigger=isTrigger.equalsIgnoreCase("TRUE");
	}
	
	public Legend(String index, String name, String collisionType, String isTrigger){
		INDEX = Integer.parseInt(index);
		NAME = name;
		this.hasCollision=!collisionType.equalsIgnoreCase("None");
		IMAGE_SRC = "";
		this.isTrigger=isTrigger.equalsIgnoreCase("TRUE");
	}

	public boolean isTrigger() {
		return isTrigger;
	}
}
