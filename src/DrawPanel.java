import javax.swing.JPanel;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class DrawPanel extends JPanel { //drawing all the frame
	private ArrayList<Integer> inputBuffer = new ArrayList<Integer>();
	
	public ArrayList<Integer> getInput(){
		return inputBuffer;
	}
	public void clearInputBuffer(){
		inputBuffer.clear();
	}
	
	public DrawPanel(){
		this.setFocusable(true);
		this.requestFocusInWindow();
		this.setBackground(new Color(51,255,255));
		keyListener();
	}


	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		//get viewport
		Camera camera = Game.getCurrentScene().getCamera(); //this is todo
		Vector2D camDiff = Game.getCurrentScene().getCamera().getOffset();
		
		//get everything that needs to be drawn
		ArrayList<EntityInterface> entitys = Game.getEntitys();
		TileMap tileMap = Game.getTileMap();
		
		//draw TileMap
		for(int x=0; x<tileMap.getWidth(); x++){
			for(int y=0; y<tileMap.getHeight(); y++){
				Tile tile = tileMap.getTile(x, y);
				int xpos = x*Tile.WIDTH, ypos=y*Tile.HEIGHT;
				
				switch(tile.getIndex()){
				case 0: g.setColor(Color.BLACK); break;
				case 1: g.setColor(new Color(139,69,19)); break;
				case 2: g.setColor(Color.GRAY); break;
				case 3: g.setColor(Color.ORANGE); break;
				}
				
				g.fillRect(xpos-camDiff.X(), ypos-camDiff.Y(), 
						Tile.WIDTH, Tile.HEIGHT);
				
				//if blank tile
				if(tile.getIndex()==-1){
					g.setColor(Color.GRAY);
					//draw box with x in the middle
					g.drawLine(xpos, ypos, xpos+Tile.WIDTH, ypos+Tile.HEIGHT);
					g.drawLine(	xpos, ypos+Tile.HEIGHT, 
								xpos+Tile.WIDTH, ypos);
					g.drawRect(xpos, ypos, Tile.WIDTH, Tile.HEIGHT);
				}
			}
		}
		
		//draw all entitiys
		for(EntityInterface e : entitys){
			ArrayList<Component> comps;
			
			//draw all sprites
			comps = e.getComponentsByType(Sprite.class.toString());
			for(Component c : comps){
				if(c.getClass().equals(Sprite.class)){
					Sprite sprite = (Sprite)c;
					g.drawImage(sprite.getImage(), 
							e.getPosition2D().X() + sprite.getOffset().X() - camDiff.X(), 
							e.getPosition2D().Y() + sprite.getOffset().X() - camDiff.Y(), 
							sprite.width(), sprite.height(), null);
				}
			}
			
			//SpriteSheets
			comps = e.getComponentsByType(SpriteSheet.class.toString());
			for(Component c : comps){
				if(c.getClass().equals(SpriteSheet.class)){
					SpriteSheet sheet = (SpriteSheet)c;
					/*
					 * g.drawImage(sheet.getImage(), 
					 							e.getPosition2D().X(),
							e.getPosition2D().Y(),
							sheet.getScreenDimension.X(),
							sheet.getScreenDimension.Y(),
							sheet.getSourcePos.X(),
							sheet.getSoruce.Y(),
							
							*/
					
					g.drawImage(sheet.getImage(), 
							e.getPosition2D().X() + sheet.getOffset().X() - camDiff.X(),
							e.getPosition2D().Y() + sheet.getOffset().Y() - camDiff.Y(),
							sheet.getDimensions().X(),
							sheet.getDimensions().Y(), null);
							
				}
			}
			
			//Dialog
			comps = e.getComponentsByType(DialogTree.class.toString());
			for(Component c : comps){
				if(c.getClass()==DialogTree.class){
					DialogTree tree = (DialogTree) c;
					//if talking display text
					if(tree.IsTalking()){
						g.drawString(tree.curText(), 5, 300);
					}
					//display prompt to start talking
					else{
						g.drawString(tree.getPrompt(), 5, 300);
					}
				}
			}
			
			//HUD
			comps = e.getComponentsByType(HudItem.class.toString());
			for(Component c : comps){
				if(c.getClass()==HudItem.class){
					HudItem item = (HudItem)c;
					if(item.getType()==HudItem.Type.image){
						if(item.getImg()!=null){
							g.drawImage(item.getImg(), item.getX(), item.getY(),
								item.getWidth(), item.getHeight(), null);
						}
					}
					else{
						g.setColor(item.getTextColor());
						g.setFont(item.getFont());
						g.drawString(item.getText(), item.getX(), item.getY());
					}
				}
				
			}
			
			//default font for debugging
			g.setFont(new Font("Arial", Font.PLAIN, 12));
			
			if(Game.DEBUG_MODE){
				//draw collisions for debugging
				g.setColor(Color.green);
				comps = e.getComponentsByType(Collision.class.toString());
				for(Component c : comps){
					if(c.getClass().equals(CollisionBox.class)){
						CollisionBox coll = (CollisionBox)c;
						g.drawRect(coll.getRect().x - camDiff.X(), 
								coll.getRect().y - camDiff.Y(), 
								coll.getRect().width, 
								coll.getRect().height);
					}
				}
			}
		}
		
		if(Game.DEBUG_MODE){
			//draw camera bounds
			g.setColor(Color.YELLOW);
			Rectangle r = camera.getBounds();
			g.drawRect(r.x-camDiff.X(), r.y-camDiff.Y(), r.width, r.height);
			g.drawString("Camera bounds", r.x-camDiff.X(), r.y-camDiff.Y());
		}
		
		if(Game.DEBUG_MODE){
			//draw runtime status
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, 200, 80);
			g.setColor(Color.green);
			ArrayList<String> status = Game.getRuntimeStatus();
			for(int i=0; i<status.size(); i++)
				g.drawString(status.get(i), 4, 10 + (i*15));
		}
	}
	
	//listener for key input
	private void keyListener() {
		this.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {}

            @Override
            public void keyReleased(KeyEvent e) {
            	int index = inputBuffer.indexOf(e.getExtendedKeyCode());
            	if(index!=-1){
            		inputBuffer.remove(index);
            	}
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(!inputBuffer.contains(e.getExtendedKeyCode())){
                	inputBuffer.add(e.getExtendedKeyCode());
                }
            }
        });
	}
	
	
}
