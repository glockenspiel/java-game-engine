import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class Sprite implements Component {
	
	private String entityTag;
	private Vector2D offset;
	private BufferedImage image;
	private String src;
	private Vector2D dimensions;
	
	//basic construction that all constructors must have
	private void basicConstructor(String tag, String imgSrc) {
		src=imgSrc;
		loadImage();
		this.entityTag=tag;
	}
	
	public Sprite(Entity e, String imgSrc, int width, int height){
		basicConstructor(e.getTag(),imgSrc);
		dimensions=new Vector2D(width,height);
		offset = new Vector2D(0,0);
	}

	public Sprite(Entity e, String imgSrc, int width, int height, int offsetX, int offsetY){
		basicConstructor(e.getTag(),imgSrc);
		dimensions=new Vector2D(width,height);
		offset = new Vector2D(offsetX,offsetY);
	}
	
	public Sprite(Entity e, String imgSrc, int width, int height, Vector2D offset){
		basicConstructor(e.getTag(),imgSrc);
		dimensions=new Vector2D(width,height);
		this.offset = offset;
	}

	private void loadImage(){
		try {
			image = javax.imageio.ImageIO.read(new File(src));
		} catch (IOException ex) {
			Game.log("ERROR: IO exception when loading Sprite: '" 
					+ src + "' \n" + ex);
		}
	}
	
	@Override
	public void initiate() {}

	@Override
	public void update() {}

	@Override
	public String getTag() {
		return "SPRITE";
	}

	@Override
	//no need to move, position is entity position
	public void move(Vector2D amount) {}

	@Override
	public String getEntityTag() {
		return entityTag;
	}
	
	//offset of image from entity position
	public Vector2D getOffset(){
		return offset;
	}

	public BufferedImage getImage() {
		return image;
	}
	
	public int width(){ return dimensions.X(); }
	public int height(){ return dimensions.Y(); }

}
