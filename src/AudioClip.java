import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.sound.sampled.*;
import javax.sound.sampled.Mixer.Info;

//for .wav, .aif, .mid only 
public class AudioClip implements Component {

	private static final int BUFFER_SIZE = 1024;
	private String entityTag;
	private boolean is3dSound=true;
	private String src;
	private File soundFile;
	private boolean isPlaying=true;
	private boolean isPaused=false;
	boolean hasStopped=false;
	
	public AudioClip(String entityTag, boolean is3dSound, String src){
		this.entityTag=entityTag;
		this.is3dSound=is3dSound;
		this.src=src;
		loadFile();
		play();
	}
	
	private void loadFile() {
		try {
            soundFile = new File(src);
        } catch (Exception e) {
        	Game.log("file not found: " + src);
        	return;
        }
	}

	public void play(){
		if(!clip.isAlive() && !hasStopped){
			clip.start();
		}
		else if(hasStopped){
			Game.log("AudioClip has stopped, you cannot play a stopped sound");
		}
    }
	
	public void stop(){
		if(clip.isAlive()){
			isPlaying=false;
			hasStopped=true;
		}
	}
	
	public void pauseAudio(){
		if(clip.isAlive())
			isPaused=true;
	}
	
	public void resumeAudio(){
		if(clip.isAlive())
			isPaused=false;
	}
	
	private Thread clip = new Thread(){	
		public void run(){
			//create audio stream
	        AudioInputStream audioStream = null;
			try {
	            audioStream = AudioSystem.getAudioInputStream(soundFile);
	        } catch (Exception e){
	            Game.log("Audio stream error: ");
	            e.printStackTrace();
	            return;
	        }
	
			
			//get the format
	        AudioFormat audioFormat = audioStream.getFormat();
	
	        //open source data
	        DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
	        SourceDataLine sourceLine = null;
			try {
	            sourceLine = (SourceDataLine) AudioSystem.getLine(info);
	            sourceLine.open(audioFormat);
	        } catch (LineUnavailableException e) {
	        	Game.log("source data line unavailable with source: " + src);
	            e.printStackTrace();
	            return;
	        } catch (Exception e) {
	            e.printStackTrace();
	            return;
	        }
			//start source data
	        sourceLine.start();
	
	        int nBytesRead = 0;
	        byte[] abData = new byte[BUFFER_SIZE];
		        while (nBytesRead != -1 && isPlaying) {
		        	if(isPaused==false){
			            try {
			                nBytesRead = audioStream.read(abData, 0, abData.length);
			            } catch (IOException e) {
			            	Game.log("warning: IO exception caught when reading audio input stream: " + src);
			                e.printStackTrace();
			            }
			            if (nBytesRead >= 0) {
			                int nBytesWritten = sourceLine.write(abData, 0, nBytesRead);
			            }
		        	}
		        }
	
	        //drain queued data and close
	        sourceLine.drain(); 
	        sourceLine.close();
		} //end of run()
	};
	
	@Override
	public void initiate() {
	}

	@Override
	public void update() {
		
	}

	@Override
	public String getTag() {
		return "AUDIO";
	}

	@Override
	public void move(Vector2D amount) {
		
	}

	@Override
	public String getEntityTag() {
		return entityTag;
	}

	public boolean IsPlaying() {
		return isPlaying;
	}

}
