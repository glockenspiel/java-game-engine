import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class ReadCSV {
	public ReadCSV(){}
	
	//return 2D arraylist of map indexes
	public ArrayList<ArrayList<Integer>> getData(String filename){
		ArrayList<ArrayList<Integer>> data = new ArrayList<ArrayList<Integer>>();
		
		Scanner sc;
		try {
			sc = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			System.out.println("ReadCSV.getData(): File not found: " + filename);
			return null;
		}
		
		
		int value;
		while(sc.hasNextLine()) {
			String line = sc.nextLine();
	        String[] lineArray = line.split(",");
	        ArrayList<Integer> row = new ArrayList<Integer>();
	        
	        for(int i=0; i<lineArray.length; i++){
	        	try{  value = Integer.parseInt(lineArray[i]); }
	        	catch(NumberFormatException e){
	        		System.out.println("ReadCSV.getData(): Number format exception with string: " 
	        				+ lineArray[i]);
	        		value=-1;
	        	}
	        	row.add(value);
	        }
	        data.add(row);
		}
		
		sc.close();
		return data;
	}

	//get csv arraylist as string
	public ArrayList<ArrayList<String>> getStringData(String filename) {
		ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>();

		Scanner sc;
		try {
			sc = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			System.out.println("ReadCSV.getData(): File not found: " + filename);
			return null;
		}
		
		int value;
		while(sc.hasNextLine()) {
			String line = sc.nextLine();
	        String[] lineArray = line.split(",");
	        ArrayList<String> row = new ArrayList<String>();
	        
	        for(int i=0; i<lineArray.length; i++){
	        	row.add(lineArray[i]);
	        }
	        data.add(row);
		}
		
		sc.close();
		return data;
	}
}
