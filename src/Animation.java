
/*Used with SpriteSheet to store a data structure 
 * which will represent animation
 * each frame position is the coordinates of
 * a sub-image in a grid
 * 
 * if frame Positions Set is less than the max
 * the animation wont play smothly
 */

public class Animation {
	private int[][] framePositions;
	private int frameCount=0;
	private final String NAME;
	
	public Animation(int maxFrameCount, String name){
		framePositions = new int[maxFrameCount][2];
		this.NAME=name;
	}
	
	//set all positions
	public void setFramePosition(int x, int y, int order){
		framePositions[order][0]=x;
		framePositions[order][1]=y;
	}
	
	public int getFramePosY(){
		return framePositions[frameCount][1];
	}
	
	public int getFramePosX(){
		return framePositions[frameCount][0];
	}

	public void nextFrame() {
		frameCount++;
		if(frameCount>=framePositions.length) {
			frameCount=0; 
		}
	}
	
	public String getName(){
		return NAME;
	}
	
	public void resetCount(){
		frameCount=0;
	}
	
	
}
