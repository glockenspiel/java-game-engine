import java.util.Random;

//game math class
public class Mathg {
	
	//random number with random seed
	public static int random(int min, int max){
		long seed=System.currentTimeMillis();
		Random r =new Random(seed);
		return r.nextInt(max-min+1) + min;
	}
	
	//random number with defined seed
	public static int random(int min, int max, long seed){
		Random r =new Random(seed);
		return r.nextInt(max-min+1) + min;
	}
	
	//clamp to min and max
	public static int clamp(int value, int max, int min){
		return Math.min(max, Math.max(min, value));
	}
	
	//lerp is a simple smoothing algorithm
	public static int lerp(int a, int b, float smoothness){
		//linear interpolation based on the following formula
		//value1 + (value2-value1) * smoothingAmount
		float result = a + (a-b)*(1/(float)smoothness); 
		return Math.round(result);
	}
	
	public static double distance(int x1, int y1, int x2, int y2){
		return Math.hypot(x1-x2, y1-y2);
	}
}
