import java.awt.Rectangle;


public class CollisionBox extends Collision {

	private int width,height;
	public Rectangle getRect(){
		return new Rectangle(super.getPosition().X(), super.getPosition().Y(), width,height);
	}
	public Vector2D getOrigin(){
		Rectangle rect = getRect();
		return new Vector2D((int)rect.getCenterX(), (int)rect.getCenterY());
	}
	
	public String getType(){
		return "BOX";
	}
	
	public CollisionBox(Entity e, int width, int height, boolean isStatic, boolean isTrigger) {
		super(e, isStatic, isTrigger);
		this.width=width;
		this.height=height;
	}
	
	public CollisionBox(Entity e, int xOffset, int yOffset, int width, int height, boolean isStatic, boolean isTrigger) {
		super(e, isStatic, isTrigger);
		super.move(new Vector2D(xOffset, yOffset));
		this.width=width;
		this.height=height;
	}
	
	public CollisionBox(Entity e, Vector2D offset, int width, int height, boolean isStatic, boolean isTrigger) {
		super(e, isStatic, isTrigger);
		super.move(offset);
		this.width=width;
		this.height=height;
	}
	
	public CollisionBox(Entity e, Rectangle rectangle, boolean isStatic, boolean isTrigger) {
		super(e, isStatic, isTrigger);
		super.move(new Vector2D(rectangle.x, rectangle.y));
		this.width=rectangle.width;
		this.height=rectangle.height;
	}
	
	public CollisionBox(Tile tile, boolean isStatic, boolean isTrigger){
		super(tile, isStatic, isTrigger);
		width=Tile.WIDTH;
		height=Tile.HEIGHT;
	}
	
	public boolean intersects(Collision coll){
		if(coll.getType()==null) {
			Game.log("collision type not set");
			return false;
		}
		else if(coll.getType().equals("BOX")){
			return intersectsBox((CollisionBox)coll);
		}
		
		return false;
	}

	private boolean intersectsBox(CollisionBox coll) {
		return getRect().intersects(coll.getRect());
	}
	
	public Vector2D depth(Collision collision) {
		
		//box,box
		if(collision.getClass()==CollisionBox.class){
			CollisionBox b = (CollisionBox)collision;
			Rectangle size = getRect().intersection(b.getRect());
			Vector2D depth =  new Vector2D(size.width-1, size.height-1);
			
			/*
			    --  | +-
			   -----|----
			    -+  | ++
			    
			    make values positive or negative depending on which size they are 
			    on of the collision passed in. 
			    Table above describes the sign values of x and y
			 */
			
			int x=1, y=1;
			
			
			if(this.getOrigin().X()<b.getOrigin().X()){
				x=-1;
			}
			if(this.getOrigin().Y()<b.getOrigin().Y()){
				y=-1;
			}
			
			depth = depth.multi(new Vector2D(x,y));
			
			return depth;
			
		}
		return null;
	}

}
