import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class HudItem implements Component {

	public enum Type {
		label,
		image
	};
	
	private int x,y,w,h;
	private String text;
	private String entityTag;
	private Type type;
	private Color textColor;
	private String imgSrc;
	private BufferedImage image;
	private TextStyle style;
	
	//label constructor
	public HudItem(String text, int x, int y, String entityTag, TextStyle style){
		this.text=text;
		this.x=x;
		this.y=y;
		this.entityTag=entityTag;
		this.type=Type.label;
		this.style=style;
	}
	
	//image constructor
	public HudItem(String imgSrc, int x, int y, int width, int height, String entityTag){
		this.x=x;
		this.y=y;
		this.entityTag=entityTag;
		w=width;
		h=height;
		this.imgSrc=imgSrc;
		type=Type.image;
		loadImage();
	}
	
	public int getX(){return x;}
	public int getY(){return y;}
	public int getWidth(){return w;}
	public int getHeight(){return h;}
	public String getText(){
		if(text==null) return "";
		return text;
	}
	public Type getType(){return type;}
	public Color getTextColor(){return style.color();}
	public String getImgSrc(){return imgSrc;}
	public Font getFont(){return style.font();}
	
	private void loadImage(){
		try {
			image = javax.imageio.ImageIO.read(new File(imgSrc));
		} catch (IOException ex) {
			Game.log("ERROR: IO exception when loading Sprite: '" 
					+ imgSrc + "' \n" + ex);
		}
	}
	
	
	@Override
	public void initiate() {
	}

	@Override
	public void update() {
	}

	@Override
	public String getTag() {
		return "HUDITEM";
	}

	@Override
	public void move(Vector2D amount) {
	}

	@Override
	public String getEntityTag() {
		return entityTag;
	}

	public BufferedImage getImg() {
		return image;
	}

}
