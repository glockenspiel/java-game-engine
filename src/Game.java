import javax.swing.*;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;
import java.util.ArrayList;


public class Game {
	private static JFrame frame = new JFrame();
	private static DrawPanel panel = new DrawPanel();
	public static boolean isRunning=true;
	public static InputReader inputReader = new InputReader();
	private static ArrayList<String> runtimeStatus = new ArrayList<String>();
	public final static boolean DEBUG_MODE =true;
	private final static long frameTime = 16;
	
	protected static final int FRAME_HEIGHT=400, FRAME_WIDTH=500;
	private static ArrayList<Scene> scenes = new ArrayList<Scene>();
	private static int currentScene=0;
	
	private static long maxFrameTime=0, minFrameTime=(int)(frameTime*4);
	
	public static void main(String[] args) {
		
		Dimension d = new Dimension(FRAME_WIDTH,FRAME_HEIGHT);
		frame.setSize(d);
		frame.add(panel);
		frame.setResizable(false);
		frame.addWindowListener(new WindowAdapter() {
		    @Override
		    public void windowClosing(WindowEvent e) { //end all threads
		    	System.out.println("Exited");
		    	System.exit(0);
		    }
		});
		addScene("main");
		loadScene("main");
		frame.setVisible(true);
		
		//main loop
		Thread thread = new Thread(){
		    public void run(){      
		      while(isRunning){ //game loop until exit
		    	  long startTime=System.currentTimeMillis();
		    	  long startTimeNano= System.nanoTime();
		    	  isRunning=inputReader.getRunningGame();
		    	  inputReader.read(panel.getInput()); //parse input
		    	  
		    	  scenes.get(currentScene).update(); //update scene
		    	  
		    	  frame.repaint();
		    	  
		    	  runtimeStatus(System.nanoTime()-startTimeNano);
		    	  
		          long  executionTime= System.currentTimeMillis()-startTime;
		          long sleepTime=frameTime-executionTime;
		    	  if(sleepTime<0) sleepTime=0; //frame execution was longer than frameTime
		    	  
		    	try {Thread.sleep(sleepTime);} 
		    	catch (InterruptedException e) {e.printStackTrace();}
		      }
		      closeGame();
		    }

			

			private void closeGame() {
				System.exit(0);
			}
		  };
		  thread.start();
	}

	private static void loadScene(String id) {
		boolean found=false;
		for(int i=0; i<scenes.size() && !found; i++)
			if(scenes.get(i).ID.equals(id)){
				Game.log("Loading scene: "+ id);
				currentScene=i;
				found=true;
			}
		if(!found) Game.log("Scene not found: " + id);
	}
	
	private static void runtimeStatus(long executionTime) {
		runtimeStatus.clear();
		executionTime/=1000;
		if(executionTime > maxFrameTime)
			maxFrameTime=executionTime;
		if(executionTime < minFrameTime)
			minFrameTime=executionTime;
		
		Runtime runtime = Runtime.getRuntime();

	    NumberFormat format = NumberFormat.getInstance();
		
	    StringBuilder sb = new StringBuilder();
	    long maxMemory = runtime.maxMemory();
	    long allocatedMemory = runtime.totalMemory();
	    long freeMemory = runtime.freeMemory();
	    
	    runtimeStatus.add("execute time: " + executionTime + "us");
	    runtimeStatus.add("min:" + minFrameTime + "us  max:" + maxFrameTime + "us");
	    runtimeStatus.add("free memory: " + format.format(freeMemory / 1024) + "KB");
	    runtimeStatus.add("allocated memory: " + format.format(allocatedMemory / 1024) + "KB");
	    runtimeStatus.add("total free memory: " + format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024) + "KB");
	}

	private static void addScene(String id) {
		//check duplicate
		boolean flag=true;
		for(int i=0; i<scenes.size() && flag; i++)
			if(scenes.get(i).ID.equals(id)){
				flag=false;
				Game.log("Scene already exists: " + id);
			}
		
		if(flag)
			scenes.add(new Scene(id));	
	}

	
	protected static TileMap getTileMap(){
		return scenes.get(currentScene).getTileMap();
	}
	
	public static void log(String msg){
		System.out.println(msg);
	}

	public static ArrayList<EntityInterface> getEntitys() {
		return scenes.get(currentScene).getEntitys();
	}
	
	public static ArrayList<String> getRuntimeStatus(){
		return runtimeStatus;
	}

	public static Scene getCurrentScene() {
		return scenes.get(currentScene);
	}

}
