import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;


public class Scene {
	public final String ID;
	private static ArrayList<EntityInterface> entitys = new ArrayList<EntityInterface>();
	private static TileMap tileMap = new TileMap("Resources/tilemap.csv", 
			"Resources/tilemapLegend.csv");
	private CollisionManager collisionMgr = new CollisionManager();
	private String currentCameraEntity="";
	
	public Scene(String id){
		ID=id;
		init();
	}
	
	public void init() {
		//creating all entities
		Entity player= new Entity("player");
		player.addComponent(new CollisionBox(player,64,64,false, false));
		//sprite example
		//player.addComponent(new Sprite(player, "Resources/Images/sand.png", 50,50));
		player.addComponent(
				new SpriteSheet(player, "Resources/Images/testSheet.png", 
						64,64,32,32, 1000));
		TextStyle style= new TextStyle(Color.GREEN, "TimesRoman", 20);
		player.addComponent(new HudItem("testing", 10, 100, player.getTag(), style));
		player.addComponent(new HudItem("Resources/Images/sand.png", 0,100,50,50,player.getTag()));
		
		player.setPosition2D(new Vector2D(160,190));
		//camera must be given starting position
		player.addComponent(new Camera(150,150, new Vector2D(50,50), player.getTag(), player.getOrigin2D()));
		entitys.add(player);
		setCurrentCamera(player.getTag());
		
		Entity sound = new Entity("Sound");
		sound.addComponent(new AudioClip(sound.getTag(), false, "Resources/Sounds/trippygaia1.mid"));
		entitys.add(sound);
		
		Entity sound2 = new Entity("Sound2");
		sound2.addComponent(new AudioClip(sound2.getTag(), false, "Resources/Sounds/1-welcome.wav"));
		entitys.add(sound2);
		
		Entity box = new Entity("Box");
		box.addComponent(new CollisionBox(box,30,30,false,true));
		DialogTree tree = new DialogTree(box.getTag(), "john");
		tree.addBranch("example branch");
		tree.addBranch("example2");
		tree.addBranch("example3");
		box.addComponent(tree);
		box.setPosition2D(new Vector2D(250,250));
		entitys.add(box);
	}

	public void update(){
		//game logic
		Vector2D playerMovement=Game.inputReader.getDirection();
		playerMovement.scale(4);
   	  	int playerIndex=getEntityIndexByTag("player");
   	  	if(playerIndex!=-1){
   	  		entitys.get(playerIndex).move(playerMovement);
   	  	}
   	  	
   	  	Component crap = entitys.get(playerIndex).getComponentByTag("SPRITESHEET");
		SpriteSheet s = (SpriteSheet)crap;
		
   	  	if(Game.inputReader.isKeyDown('E'))
	  		s.changeAnimation("backWards");
   	  	else s.changeAnimation("example");
   	  	
   	  	
   	  	Component music = getEntityByTag("Sound").getComponentByTag("AUDIO");
   	  	AudioClip a = (AudioClip) music;
   	  	if(Game.inputReader.isKeyDown('P')){
   	  		a.stop();
   	  	}
   	  	
   	  	Component voice = getEntityByTag("Sound2").getComponentByTag("AUDIO");
	  	AudioClip b = (AudioClip) voice;
	  	if(Game.inputReader.isKeyDown('U')){
	  		b.stop();
	  	}
	  	else if(Game.inputReader.isKeyDown('I')){
	  		b.play();
	  	}
	  	
	  	Entity ent = (Entity)getEntityByTag("Box");
	  	Component triggerBox=ent.getComponentsByType(CollisionBox.class.toString()).get(0);
	  	CollisionBox tbox = (CollisionBox)triggerBox;
	  	Entity player = (Entity)getEntityByTag("player");
	  	Component playBox = (CollisionBox)player.getComponentsByType(CollisionBox.class.toString()).get(0);
	  	CollisionBox pBox = (CollisionBox)playBox;
	  	
	  	if(tbox.intersects(pBox)){
		  	Component compon = ent.getComponentsByType(DialogTree.class.toString()).get(0);
		  	DialogTree t = (DialogTree)compon;
		  	if(Game.inputReader.isKeyClicked('T')){
		  		if(t.IsTalking()==false)
		  			t.startTalking();
		  		else
		  			t.nextBranch();
		  	}
	  	}
   	  	
   	  	//collision
   	  	
   	  	//get all collision object types
   	  	ArrayList<Collision> colls = new ArrayList<Collision>();
   	  	for(EntityInterface e : entitys){
   	  	ArrayList<Component> entityColls = e.getComponentsByType(Collision.class.toString()); //e.getCollisions("COLLISION"); //e.getComponentsByType("COLLISION");//e.getComponentsByTag("COLLISION");
   	  		for(Component c : entityColls)
   	  			colls.add((Collision)c);
   	  	}
   	  	
   	  	colls.addAll(tileMap.getAllCollisions());
   	  	
   	  	collisionMgr.resolve(colls);
   	  	
   	  	
   	  	for(EntityInterface e : entitys){ //set all game logic values
   	  		e.update();
   	  	}
	}
	


	public static int getEntityIndexByTag(String tag){
		for(int i=0; i<entitys.size(); i++)
			if(entitys.get(i).getTag().equalsIgnoreCase(tag))
				return i;
		return -1;
	}
	
	public static EntityInterface getEntityByTag(String tag){
		for(EntityInterface e : entitys)
			if(e.getTag().equalsIgnoreCase(tag))
				return e;
		return null;
	}
	
	protected static ArrayList<EntityInterface> getEntitys(){
		return entitys;
	}
	
	protected static TileMap getTileMap(){
		return tileMap;
	}
	
	private void setCurrentCamera(String tag) {
		currentCameraEntity=tag;
	}

	public Camera getCamera() {
		Component comp =  getEntityByTag(currentCameraEntity).getComponentByTag("CAMERA");
		if(comp!=null){
			Camera c = (Camera)comp;
			return c;
		}
		return null;
	}
	
	
	
}
